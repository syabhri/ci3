<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function index()
	{
		//$this->load->helper('url');

		$data['title'] = "About";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('about_view');
		$this->load->view('templates/footer');
	}
}
