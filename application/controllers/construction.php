<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Construction extends CI_Controller {

	public function index()
	{
		$data['title'] = "under construction";

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('templates/construction');
		$this->load->view('templates/footer');
	}

}

/* End of file construction.php */
/* Location: ./application/controllers/construction.php */