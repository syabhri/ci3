<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//$this->load->helper('url');
		$this->load->model('user_model');
		$this->load->library('form_validation');

	}

	//register user
	public function register()
	{
		$data['title'] = "Registration Form";

		//set form re-population
		$data['nama'] = $this->input->post('nama');
		$data['email'] = $this->input->post('email');
		$data['kodepos'] = $this->input->post('kodepos');

		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password2', 'Konfirmasi Password', 'matches[password]');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('templates/navbar');
			$this->load->view('users/register', $data);
			$this->load->view('templates/footer');
		} else {
			$enc_password = sha1($this->input->post('password'));

			$this->user_model->register($enc_password);

			// Set message
			$this->session->set_flashdata('user_registered', 'Registrasi Anda Berhasil.');

			redirect('blog');
		}

	}

	// login user
	public function login()
	{
		$data['title'] = "Login Form";

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('templates/navbar');
			$this->load->view('users/login', $data);
			$this->load->view('templates/footer');
		} else {
			
			//get login data
			$username = $this->input->post('username');
			$password = sha1($this->input->post('password'));

			// login with login data
			$user_id = $this->user_model->login($username, $password);

			//if success create session
			if($user_id){
				// Buat session
				$user_data = array(
					'user_id' => $user_id,
					'username' => $username,
					'logged_in' => true,
					'level' => $this->user_model->get_user_level($user_id),
				);

				$this->session->set_userdata($user_data);

				// Set message
				$this->session->set_flashdata('user_loggedin', 'Anda sudah login');

				redirect('user/dashboard');
			} else {
				// Set message
				$this->session->set_flashdata('login_failed', 'Username atau password salah');

				redirect('user/login');
			}		
		}
	}

	// Log out user
	public function logout(){
		// Unset user data
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('username');

		// Set message
		$this->session->set_flashdata('user_loggedout', 'Anda sudah log out');

		redirect('user/login');
	}

	// load halaman Dashboard
	function dashboard()
	{
		// check session
		if(!$this->session->userdata('logged_in')) 
			redirect('user/login');

		$user_id = $this->session->userdata('user_id');

		// Dapatkan detail dari User
		$data['user'] = $this->user_model->get_user_details( $user_id );

		// Load view
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('users/dashboard', $data);
		$this->load->view('templates/footer');
	}
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */