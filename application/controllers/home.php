<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		//$this->load->helper('url');

		$data['title'] = "Welcome To The Site";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar');
		$this->load->view('home_view');
		$this->load->view('templates/footer');
	}
}
