<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
   <div class="py-5 text-center">
       <h2>Selamat datang <?php echo $user->nama ?> <span class="badge badge-secondary"><?php echo $user->name ?></span></h2>
   </div>
   <?php if($user->level_id == '1') : ?>
              <div class="btn-group" role="group" aria-label="Data baru">
                  <?php echo anchor('user/create', 'Create', array('class' => 'btn btn-outline-light')); ?>
              </div>

          <?php endif; ?>
</div>
