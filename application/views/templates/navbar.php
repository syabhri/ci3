<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url() ?>/home">Home
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url() ?>/about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
          </ul>
          
          <!-- user menu -->
          <?php if(!$this->session->userdata('logged_in')) : ?>

              <div class="btn-group" role="group" aria-label="Data baru">
                  <?php echo anchor('user/register', 'Register', array('class' => 'btn btn-outline-light')); ?>
                  <?php echo anchor('user/login', 'Login', array('class' => 'btn btn-outline-light')); ?>

              </div>

          <?php endif; ?>

          <?php if($this->session->userdata('logged_in')) : ?>
              <div class="btn-group" role="group" aria-label="Data baru">

                  <?php //echo anchor('blog/create', 'Artikel Baru', array('class' => 'btn btn-outline-light')); ?>
                  <?php //echo anchor('category/create', 'Kategori Baru', array('class' => 'btn btn-outline-light')); ?>
                  <?php echo anchor('user/logout', 'Logout', array('class' => 'btn btn-outline-light')); ?>
              </div>

          <?php endif; ?>

        </div>
      </div>
    </nav>

    <!-- alert bar -->
    <?php if($this->session->flashdata('user_registered')): ?>
      <?php echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('user_registered').'</div>'; ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('login_failed')): ?>
      <?php echo '<div class="alert alert-danger">'.$this->session->flashdata('login_failed').'</div>'; ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('user_loggedin')): ?>
      <?php echo '<div class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</div>'; ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('user_loggedout')): ?>
      <?php echo '<div class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</div>'; ?>
    <?php endif; ?>