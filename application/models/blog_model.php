<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	//mengabil semua artikel
	public function get_all_artikel( $limit = FALSE, $offset = FALSE ) 
    {
        if ( $limit ) {
            $this->db->limit($limit, $offset);
        }
 
        $this->db->order_by('blogs.post_date', 'DESC');

        //$this->db->join('categories', 'categories.cat_id = blogs.fk_cat_id');
        
        $query = $this->db->get('blogs');

    	return $query->result();
    }

    //mengambil artikel sesuai id
    public function get_artikel_by_id($id)
    {
        /*$this->db->select ( '
            blogs.*, 
            categories.cat_id as category_id, 
            categories.cat_name,
            categories.cat_description,
        ' );
        $this->db->join('categories', 'categories.cat_id = blogs.fk_cat_id');
		*/

    	$query = $this->db->get_where('blogs', array('blogs.post_id' => $id));
    	            
		return $query->row();
    }

    //mengabil artikel sesuai slug
    public function get_artikel_by_slug($slug)
    {

         // Inner Join dengan table Categories
        $this->db->select ( '
            blogs.*, 
            categories.cat_id as category_id, 
            categories.cat_name,
            categories.cat_description,
        ' );
        $this->db->join('categories', 'categories.cat_id = blogs.fk_cat_id');
        
        $query = $this->db->get_where('blogs', array('post_slug' => $slug));

        // Karena datanya cuma 1, kita return cukup via row() saja
        return $query->row();
    }

    //menyimpan artikel yang di buat ke database
    public function create_artikel($data)
    {
        return $this->db->insert('blogs', $data);
    }

}

/* End of file blog_model.php */
/* Location: ./application/models/blog_model.php */